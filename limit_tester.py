import requests
from pandas_ods_reader import read_ods
import numpy as np
import pandas as pd
from IPython.display import display
import random
import time

DM_API_KEY = "AIzaSyDjJCFE8mdpIYv4AapWhQQDPK2w1i5lw7k"
RP_API_KEY = "AIzaSyBR53ic_1Rvaokoglx5SKR2N0i_v4sWGPo"
RANDOM_MAGNITUDE = 5000
P = 5

def sendDMRequest():
    url = "https://maps.googleapis.com/maps/api/distancematrix/json?destinations="+\
    str(40.0+random.randrange(-1*RANDOM_MAGNITUDE, RANDOM_MAGNITUDE)/1000)+"%2C"+\
    str(-100.0+random.randrange(-1*RANDOM_MAGNITUDE, RANDOM_MAGNITUDE)/1000)+\
    "&origins="+\
    str(40.0+random.randrange(-1*RANDOM_MAGNITUDE, RANDOM_MAGNITUDE)/1000)+"%2C"+\
    str(-100.0+random.randrange(-1*RANDOM_MAGNITUDE, RANDOM_MAGNITUDE)/1000)+\
    "&key="+DM_API_KEY

    payload = {}

    headers = {}

    start = time.time()
    response = requests.request("POST", url, headers=headers, json=payload)
    stop = time.time()
    elapsed = stop - start
    if response.status_code != 200:
        print(response.content)
    return elapsed

def sendRPRequest():
    url = "https://routespreferred.googleapis.com/v1:computeRouteMatrix?key="+RP_API_KEY

    payload = {
        "origins": [generateWaypoint()],
        "destinations": [generateWaypoint()],
        "travelMode": "DRIVE"
    }

    headers = {"X-Goog-FieldMask": "*"}

    start = time.time()
    response = requests.request("POST", url, headers=headers, json=payload)
    stop = time.time()
    elapsed = stop - start
    if response.status_code != 200:
        print(response.content)
    return elapsed

# def readAddressToDict():
#     path = "sample_addresses_from_same_zone.ods"
#     df = read_ods(path)
#     return df.to_dict()

# def convertDictToWaypoints(addressDict):
#     waypoints = []
#     for i in range(0, len(addressDict['LOC_ADDRESS'])):
#         print(addressDict['LOC_ADDRESS'])
#         waypoint = {
#             "waypoint": {
#                 "location": {
                    
#                 }
#             }
#         }
#         waypoints.append(waypoint)
#     return waypoints

# def generateLatLongs():
#     waypoints = []
#     for _ in range(0, 10):
#         waypoint = [
#             40.0+random.randrange(-500, 500)/1000,
#             -100.0+random.randrange(-500, 500)/1000
#         ]
#         waypoints.append(waypoint)
#     return waypoints

def generateWaypoint():
    waypoint = {
        "waypoint": {
            "location": {
                "latLng":
                {
                    "latitude": 40.0+random.randrange(-1*RANDOM_MAGNITUDE, RANDOM_MAGNITUDE)/1000,
                    "longitude": -100.0+random.randrange(-1*RANDOM_MAGNITUDE, RANDOM_MAGNITUDE)/1000
                }
            }
        }
    }
    return waypoint

if __name__=="__main__":
    average = 0
    for i in range(0, P):
        elapsed = sendDMRequest()
        average = average + elapsed
    average = average / P
    print("Distance Matrix time elapsed on average: "+str(average))

    average = 0
    for _ in range(0, P):
        elapsed = sendRPRequest()
        average = average + elapsed
    average = average / P
    print("Routes Preferred time elapsed on average: "+str(average))